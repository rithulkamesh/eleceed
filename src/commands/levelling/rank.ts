import { PermissionFlagsBits, SlashCommandBuilder } from "discord.js";
import path from "path";
import { Eleceed } from "eleceed";
import { RankCard } from "utils/RankCard";

module.exports = {
  data: new SlashCommandBuilder()
    .setName("rank")
    .setDescription("REEEEEE")
    .setDefaultMemberPermissions(
      PermissionFlagsBits.Administrator | PermissionFlagsBits.ManageMessages
    ),
  async execute(client: Eleceed, interaction: any) {
    const mem = await client.db.member.findFirst({
      where: {
        id: interaction.user.id,
      },
    });

    if (!mem)
      return interaction.reply("No data available");

    let nextLevelXpRequired =
      5 * mem.level ** 2 + 50 * mem.level + 100;

    let lastLevelXpRequired =
      5 * (mem.level - 1) ** 2 + 50 * (mem.level - 1) + 100;
      
    const rankCard = await new RankCard()
      .setAvatar(interaction.user.displayAvatarURL({ format: "png" }))
      .setXp(mem.xp - lastLevelXpRequired)
      .setXpRequired(nextLevelXpRequired)
      .setLevel(mem.level)
      .setUsername(interaction.user.username)
      .setDiscrim(interaction.user.discriminator)
      .build();
   
    interaction.reply({files : [rankCard]})
  },
};
