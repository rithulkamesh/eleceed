import {
  ActionRowBuilder,
  SelectMenuBuilder,
  PermissionFlagsBits,
  SlashCommandBuilder,
  SlashCommandRoleOption,
  SlashCommandStringOption,
  Interaction,
  MessageInteraction,
} from "discord.js";
import { Eleceed } from "eleceed";
import { Embed } from "utils/embed";

module.exports = {
  data: new SlashCommandBuilder()
    .setName("sendrolemenu")
    .setDescription("Create a dropdown in which users can select roles")
    .setDefaultMemberPermissions(PermissionFlagsBits.Administrator),
  async execute(client: Eleceed, interaction: any) {
    const embed = new Embed()
      .setTitle("Role Menu")
      .setAuthor({
        name: "Rithul's Tech Space",
        iconURL: interaction.guild?.iconURL() || "",
      })
      .setDescription(
        "Select a role to add/remove from yourself. If you already have the role, it will be removed."
      );
    const row = new ActionRowBuilder().addComponents(
      new SelectMenuBuilder()
        .setCustomId("role_menu")
        .setPlaceholder("Nothing selected")
        .addOptions(
          {
            label: "Remove all roles...",
            description: "Remove all roles from the yourself.",
            value: "remove",
          },
          {
            label: "Fateway Pings",
            description: "Pings for Fateway updates.",
            value: "fateway",
          },
          {
            label: "General Notifications",
            description: "Pings for blog posts, Video Notifications, etc...",
            value: "general",
          }
        )
    );

    await interaction.channel?.send({ embeds: [embed], components: [row] });
    await interaction.reply({ content: "Role Menu sent!", ephemeral: true });
  },
};
