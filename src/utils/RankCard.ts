import {
  Canvas,
  SKRSContext2D,
  createCanvas,
  loadImage,
} from "@napi-rs/canvas";
import { Presence, Status } from "discord.js";
import path from "path";

export class RankCard {
  canvas: Canvas;
  ctx: SKRSContext2D;
  avatar: string = "";
  xpRem: number = 0;
  xpReq: number = 0;
  level: number = 0;
  username: string = "";
  discriminator: string = "";
 nextLevelXpRequired : number = 0;

  constructor() {
    this.canvas = createCanvas(934, 282);
    this.ctx = this.canvas.getContext("2d");
  }

  setAvatar(avatar: string): RankCard {
    this.avatar = avatar;
    return this;
  }

  setXp(xp: number): RankCard {
    this.xpRem = xp;
    return this;
  }

  setXpRequired(xp: number): RankCard {
    this.xpReq = xp;
    return this;
  }

  setLevel(level: number): RankCard {
    this.level = level;
    this.nextLevelXpRequired = 5 * level ** 2 + 50 * level + 100;
    return this;
  }


  setUsername(username: string): RankCard {
    this.username = username;
    return this;
  }

  setDiscrim(discriminator: string): RankCard {
    this.discriminator = discriminator;
    return this;
  }

  async build(): Promise<Buffer> {
    // Draw the background

    const formatter=  Intl.NumberFormat('en-US', { notation: 'compact' });
    const background = await loadImage(
      path.join(__dirname, "..", "assets", "rank_temp.png")
    );
    this.ctx.drawImage(background, 0, 0, this.canvas.width, this.canvas.height);

    this.ctx.save();

    // Drawing the Avatar
    this.ctx.beginPath();
    this.ctx.arc(125,125, 100, 0, Math.PI * 2, true);
    this.ctx.closePath();
    this.ctx.fillStyle = "#000";
    this.ctx.fill();

    this.ctx.beginPath();
    this.ctx.arc(125, 125, 90, 0, Math.PI * 2, true);
    this.ctx.clip();
    const avatar = await loadImage(this.avatar);
    this.ctx.drawImage(avatar, 25, 25, 200, 200);
    this.ctx.restore()
    this.ctx.closePath();


    // Draw a circle for the presence
    this.ctx.beginPath();
    this.ctx.arc(215, 180, 25, 0, Math.PI * 2, true);
    this.ctx.closePath();
    this.ctx.fillStyle = "#000";
    this.ctx.fill();
    
    this.ctx.beginPath();
    this.ctx.arc(215, 180, 20, 0, Math.PI * 2, true);
    this.ctx.closePath();
    this.ctx.fillStyle = "#43b581";
    this.ctx.fill();

    
    // Drawing the progress bar
    this.ctx.strokeStyle = "#000";
    this.ctx.lineWidth = 2;
    this.ctx.beginPath();
    this.ctx.roundRect(280, 165, 600, 50, 100);
    this.ctx.fillStyle = "#484b4e";
    this.ctx.stroke();
    this.ctx.fill();
    this.ctx.beginPath();
    this.ctx.roundRect(280, 165, 600 * (this.xpRem / this.xpReq), 50, 100);
    this.ctx.fillStyle = "#ff9156";
    this.ctx.fill();

    // Render Username above the progress bar, and the discriminator in a lighter color and a bit smaller
    this.ctx.font = "35px Roboto";
    this.ctx.fillStyle = "#fff";
    // Trim usernae if it's too long
    this.ctx.fillText((() => {
        if (this.username.length > 10) {
            return this.username.slice(0, 7) + "...";
        } else {
            return this.username;
        }
    })(), 280, 140);
    this.ctx.font = "25px Roboto";
    this.ctx.fillStyle = "#b9bbbe";
    this.ctx.fillText(`#${this.discriminator}`, 405, 140);

    // Render the XP required in white and the total XP required in a lighter color, above the end of the progress bar
    var start = 660
    this.ctx.font = "35px Roboto";
    this.ctx.fillStyle = "#fff";
    this.ctx.fillText(`${formatter.format(this.xpRem)}`, start, 140);
    this.ctx.fillStyle = "#b9bbbe";
    this.ctx.fillText(`/ ${formatter.format(this.nextLevelXpRequired)} XP`, start + 65, 140);


    start = 730
    this.ctx.font = "28px Roboto";
    this.ctx.fillStyle = "#fff";
    this.ctx.fillText("LEVEL", start, 80);
    this.ctx.font = "50px Roboto";
    this.ctx.fillStyle = "#fff";
    this.ctx.fillText(this.level.toString(), start + 90, 80);

    return this.canvas.toBuffer("image/png");
  }
}
