import { Eleceed } from "eleceed";
import { Events, GuildMember, Interaction, TextChannel } from "discord.js";

export default class InteractionCreate {
  public event = Events.InteractionCreate;

  public async run(client: Eleceed, interaction: any) {
    if (interaction.customId === "role_menu") {
      const selected = interaction.values[0];
      switch (selected) {
        case "remove":
          await interaction.member.roles.remove("1042073123723423764");
          await interaction.member.roles.remove("1065331005801635850");
          await interaction.reply({
            content: `Removed all roles from you.`,
            ephemeral: true,
          });
          break;
        case "fateway": {
          if (interaction.member.roles.cache.has("1065331005801635850")) {
            await interaction.member.roles.remove("1065331005801635850");
            await interaction.reply({
              content: `Removed the role from you.`,
              ephemeral: true,
            });
            return;
          }
          await interaction.member.roles.add("1065331005801635850");
          break;
        }
        case "general": {
          if (interaction.member.roles.cache.has("1042073123723423764")) {
            await interaction.member.roles.remove("1042073123723423764");
            await interaction.reply({
              content: `Removed the role from you.`,
              ephemeral: true,
            });
            return;
          }
          await interaction.member.roles.add("1042073123723423764");
          break;
        }
      }
    }

    if (!interaction.isChatInputCommand()) return;
    const command = client.commands.get(interaction.commandName);

    if (!command) {
      console.error(
        `No command matching ${interaction.commandName} was found.`
      );
      return;
    }

    try {
      //@ts-ignore
      await command.execute(client, interaction);
    } catch (error) {
      console.error(error);
      await interaction.reply({
        content: "There was an error while executing this command!",
        ephemeral: true,
      });
    }
  }
}
