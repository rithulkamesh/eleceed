import { Eleceed } from "eleceed";
import { Events, Message } from "discord.js";

export default class MessageCreateEvent {
  public event = Events.MessageCreate;

  public async run(client: Eleceed, message: Message) {
    if (message.author.bot) return;
    // If user not in database, create them
    const user = await client.db.member.findUnique({
      where: {
        id: message.author.id,
      },
    });
    if (!user) {
      await client.db.member.create({
        data: { id: message.author.id, Thanks: 0 },
      });
    }

    if (
      [
        "1042060286305239041",
        "1062776162067173477",
        "1042062704543793192",
      ].includes(message.channel.id as never)
    )
      return;

    let randXpAmount = Math.ceil(Math.random() * 10) + 10;

    const transactionResult = await client.db.$transaction([
      client.db.$executeRaw`
        UPDATE "Member" SET xp = xp + ${randXpAmount}, "lastXpCheck" = now()
        WHERE id = ${message.author.id} AND
        now() - "lastXpCheck" > '1 minute'::interval`,
      client.db.$executeRaw`
          UPDATE "Member"
            SET level = level + 1
          WHERE
            id = ${message.author.id} AND
            xp >= (5 * power(level, 2)) + (50 * level) + 100`,
    ]);

    if (transactionResult[1] > 0) {
      const user = await client.db.member.findFirst({
        where: {
          id: message.author.id,
        },
      });
      if (user) {
        await message.channel.send({
          content: `<@${user?.id}> has leveled to Level ${user.level}!`,
        });
      }
    }
  }
}
